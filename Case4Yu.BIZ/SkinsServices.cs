﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Case4Yu.DAL;
using Case4Yu.ENT;

namespace Case4Yu.BIZ
{
    class SkinsServices
    {
            public class skinsBIZ : IBIZ<skins>
        {
            public List<skins> GetAll()
            {
                return new DAL.SkinsDAL().GetAll();
            }

            public void Adicionar(skins item)
            {
                DAL.SkinsDAL da = new DAL.SkinsDAL();
                da.Adicionar(item);
            }

            public void Eliminar(skins item)
            {
                throw new NotImplementedException();
            }

            public void Actualizar(skins nuevoitem, skins originalitem)
            {
                DAL.SkinsDAL da = new DAL.SkinsDAL();
                da.Actualizar(nuevoitem, originalitem);
            }
        }
    }
}
