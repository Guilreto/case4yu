﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Case4Yu.BIZ
{

        public interface IBIZ<T>
        {
            List<T> GetAll();
            void Adicionar(T item);
            void Eliminar(T item);
            void Actualizar(T nuevoitem, T originalitem);
        }
}

