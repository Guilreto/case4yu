﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Case4Yu.ENT;

namespace Case4Yu.DAL
{

    public class SkinsDAL : IDAL<csgo_Skin>
    {
        #region Skins de IDataAccess<skins>
        public List<csgo_Skin> GetAll()
        {
            using (Case4YuContext context = new Case4YuContext())
            {
                var skins = (from skinTable in context.csgo_Skins
                             select skinTable).ToList();

                return skins;
            }
        }

        public int Adicionar(csgo_Skin item)
        {
            int result = 0;
            using (Case4YuContext context = new Case4YuContext())
            {
                context.csgo_Skins.Attach(item);
                context.Entry(item).State = System.Data.Entity.EntityState.Added;

                result = context.SaveChanges();
            }

            return result;
        }

        public int Eliminar(csgo_Skin item)
        {
            int result = 0;
            using (Case4YuContext context = new Case4YuContext())
            {
                context.csgo_Skins.Remove(item);
                context.Entry(item).State = System.Data.Entity.EntityState.Deleted;

                result = context.SaveChanges();
            }

            return result;
        }
        public int Actualizar(csgo_Skin item)
        {
            int result = 0;
            using (Case4YuContext context = new Case4YuContext())
            {
                context.csgo_Skins.Attach(item);
                context.Entry(item).State = System.Data.Entity.EntityState.Modified;

                result = context.SaveChanges();
            }

            return result;
        }

        #endregion
    }
}
