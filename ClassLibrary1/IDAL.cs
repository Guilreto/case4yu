﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Case4Yu.DAL
{
    public interface IDAL<T>
    {
        List<T> GetAll();
        int Adicionar(T item);
        int Eliminar(T item);
        int Actualizar(T item);

    }

}
