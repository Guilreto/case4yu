﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Case4YuNew.BIZ.Startup))]
namespace Case4YuNew.BIZ
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
