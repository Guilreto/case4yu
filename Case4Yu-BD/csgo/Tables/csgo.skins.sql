﻿CREATE TABLE [csgo].[skins] (
    [csgo_id] INT           IDENTITY (1, 1) NOT NULL,
    [csgo_stock]       INT           NULL,
    [csgo_precio]      BIGINT        NOT NULL,
    [csgo_skin]       NVARCHAR (50) NOT NULL,
    [csgo_imagen]  IMAGE         NULL,
    CONSTRAINT [PK_csgo.skins] PRIMARY KEY CLUSTERED ([csgo_id] ASC)
);

