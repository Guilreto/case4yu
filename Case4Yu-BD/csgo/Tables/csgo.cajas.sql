﻿CREATE TABLE [csgo].[cajas] (
    [csgo_id]     INT           IDENTITY (1, 1) NOT NULL,
    [csgo_stock]  INT           NOT NULL,
    [csgo_precio] BIGINT        NOT NULL,
    [csgo_caja]        NVARCHAR (50) NOT NULL,
    [csgo_imagen]  IMAGE         NOT NULL,
    CONSTRAINT [PK_csgo.cajas] PRIMARY KEY CLUSTERED ([csgo_id] ASC)
);

