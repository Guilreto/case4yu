﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Case4YuNew.Startup))]
namespace Case4YuNew
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
