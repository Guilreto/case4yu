﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Case4YuNew.DAL.Startup))]
namespace Case4YuNew.DAL
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
